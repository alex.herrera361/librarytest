const express = require('express');
const router = express.Router();
var mysql = require('mysql');

//Database conection
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'libs',
    port: 3307
});

//Books register
router.post('/add', async function (req, res) {
    try {
        const ql = "INSERT INTO collections SET ?";
        //Check if books and users exist
        var bduser = false;
        var bdbook = false;

        connection.query(`SELECT * FROM users WHERE users_id = ${req.body.users_id}`, function (err, rows, fields) {
            if (err) throw err;
            if (rows.length > 0) {

                bduser = true
            }
        });
        connection.query(`SELECT * FROM books WHERE users_id = ${req.body.users_id}`, function (err, rows, fields) {
            if (err) throw err;
            if (rows.length > 0) {
                bdbook = true
            }
        });
        const collectionObj = {}
        if (bduser && bdbook){
         collectionObj = {
            books_id: req.body.books_id,
            users_id: req.body.users_id
        };
        }else return "Invalid book or user"

        connection.query(ql, boockObj, error => {
            if (error) throw error;

            res.send('Collection created');
        });


    } catch (error) {
        console.log(error)
        res.status(500).json({
            data: "No result"
        });
    }
});

//Delete assignments
router.delete('/delete/:idBook', async function (req, res) {
    const { idBook } = req.params;

    try {
        const ql = `DELETE FROM books WHERE id = ${idBook}`;

        connection.query(ql, error => {
            if (error) throw error;

            res.send('Book DELETED');
        });


    } catch (error) {
        res.status(500).json({
            data: "No result"
        });
    }
});

module.exports = router;