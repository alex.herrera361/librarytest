const express = require('express');
const router = express.Router();
var mysql = require('mysql');

//Database conection
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'libs',
    port: 3307
});

//GET all books
router.get('/', async function (req, res, next) {
    try {

        connection.query('SELECT * FROM books', function (err, rows, fields) {
            if (err) throw err;
            res.status(201).json({
                data: rows
            });
        });

    } catch (error) {
        res.status(500).json({
            data: error
        });
    }
});

//GET books 4 user
router.get('/user/:id', async function (req, res, next) {
    const { id } = req.params
    try {
        connection.query("SELECT * FROM users u INNER JOIN collections c ON u.id = c.users_id INNER JOIN books b ON b.id = c.books_id WHERE u.id = " + id + ";", function (err, rows, fields) {
            if (err) throw err;
            if (rows.length > 0) {
                res.status(201).json({
                    data: rows
                });
            } else {
                res.status(201).json({
                    data: "No result"
                });
            }

        });
    } catch (error) {
        res.status(404).json({
            data: "No result"
        });
    }
});

//Books register
router.post('/add', async function (req, res) {
    try {
        const ql = "INSERT INTO books SET ?";
        const boockObj = {
            isbn: req.body.isbn,
            title: req.body.title,
            author: req.body.author,
            date: req.body.date,
            user_id: req.body.user_id
        };

        connection.query(ql, boockObj, error => {
            if (error) throw error;

            res.send('Book created');
        });


    } catch (error) {
        console.log(error)
        res.status(500).json({
            data: "No result"
        });
    }
});

//Books updater
router.put('/update/:idBook', async function (req, res) {
    const { idBook } = req.params;
    const { author, title, isbn } = req.body;
    try {
        const ql = `UPDATE books SET isbn = '${isbn}', title = '${title}', author = '${author}' WHERE id = ${idBook}`;
        connection.query(ql, error => {
            if (error) throw error;

            res.send('Book updated');
        });


    } catch (error) {
        res.status(500).json({
            data: "No result"
        });
    }
});

//Books delet
router.delete('/delete/:idBook', async function (req, res) {
    const { idBook } = req.params;

    try {
        const ql = `DELETE FROM books WHERE id = ${idBook}`;

        connection.query(ql, error => {
            if (error) throw error;

            res.send('Book DELETED');
        });


    } catch (error) {
        res.status(500).json({
            data: "No result"
        });
    }
});

module.exports = router;