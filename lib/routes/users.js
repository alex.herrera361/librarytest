const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');
var mysql = require('mysql');

//Database conection
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'libs',
    port: 3307
});

//GET all users
router.get('/', async function (req, res, next) {
    try {

        connection.query('SELECT * FROM users', function (err, rows, fields) {
            if (err) throw err;
            res.status(201).json({
                data: rows
            });
        });

    } catch (error) {
        res.status(500).json({
            data: "No response"
        });
    }
});

//GET specific user
router.get('/find/:id', async function (req, res, next) {
    const { id } = req.params
    try {
        connection.query("SELECT * FROM users WHERE id = " + id + ";", function (err, rows, fields) {
            if (err) throw err;
            if (rows.length > 0) {
                res.status(201).json({
                    data: rows
                });
            } else {
                res.status(201).json({
                    data: "No result"
                });
            }

        });
    } catch (error) {
        res.status(404).json({
            data: "No result"
        });
    }
});

//Users register

router.post('/add', async function (req, res) {
    try {
        

        var pass = req.body.password;

        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                next(err);
            }
            bcrypt.hash(pass, salt, null, (err, hash) =>{
                if (err) {
                    next(err)
                }
                const ql = "INSERT INTO users SET ?";
                const userObj = {
                    name: req.body.name,
                    email: req.body.email,
                    password: hash
                };
                connection.query(ql, userObj, error => {
                    if (error) throw error;
        
                    res.send('User created');
                });
            });
        })


    } catch (error) {
        console.log(error)
        res.status(500).json({
            data: "No result"
        });
    }
});

//Users updater
router.put('/update/:idUser', async function (req, res) {
    const { idUser } = req.params;
    const { name, email, password } = req.body;
    try {
        const ql = `UPDATE users SET name = '${name}', email = '${email}', password = '${password}' WHERE id = ${idUser}`;
        connection.query(ql, error => {
            if (error) throw error;

            res.send('User updated');
        });


    } catch (error) {
        res.status(500).json({
            data: "No response"
        });
    }
});

//User delet
router.delete('/delete/:idUser', async function (req, res) {
    const { idUser } = req.params;

    try {
        const ql = `DELETE FROM users WHERE id = ${idUser}`;

        connection.query(ql, error => {
            if (error) throw error;

            res.send('User DELETED');
        });


    } catch (error) {
        res.status(500).json({
            data: "No result"
        });
    }
});

module.exports = router;