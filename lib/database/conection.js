const Mysql = require('mysql');

class MYSQLConnection {
  constructor() {
    this.client = Mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'libs',
      port: 3307
    });
  }

  async query(querys) {
    //await this.client.connect();
    await this.client.connect();
var tp
    const data = await this.client.query(querys, function (err, rows, fields) {
      if (err) throw err;
      tp = rows
      //console.log('The solution is: ', rows[0].title);
    });
    return tp;
    this.client.end();
  }
}
module.exports = MYSQLConnection;